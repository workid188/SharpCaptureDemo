感谢您选择稳定、强大、易用的摄像头、话筒、声卡、混音类库SharpCapture!

关于使用：
    本类库不需要注册，在项目中引用SharpCapture.dll，同时将SGSupport.dll和SGSupport64.dll复制到和SharpCapture.dll同目录下即可。
    请务必保证SharpCapture.dll、SGSupport.dll和SGSupport64.dll三个dll同时发布，并位于同一个目录下！

关于试用版：
     您可以免费下载试用SharpCapture。试用版有功能和时间限制。
          1、功能限制：所有的采集器只能连续工作10分钟，10分钟后，将无数据返回。
          2、时间限制：SharpCapture试用版下载后，只能运行90天，90天后将无法运行。

关于授权码：
      我司为您提供了灵活的授权方式，详情可以联系深果智能科技。
      商务QQ：
                3535600244
      商务邮箱：
                 sales@zzsgzn.com
      商务热线：
                 187-9027-3525

深果，让C#开发更快更简单! 更多.NET开发类库，请访问深果官网:   http://www.zzsgzn.com/?SharpCaptureDemo